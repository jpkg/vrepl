use strict;
use warnings;
use diagnostics;

use File::Basename;
use lib dirname(__FILE__);
use Parse;
use Msg;
use feature 'say';

package Vrepl;

sub _tf {
    my ($first, $second) = @_;
    return Msg::color("blue", "definition of")
            . " $first\n"
            . Msg::color("green", "=> ");
}

sub _df {
    my ($first,$second) = @_;
    return Msg::color("blue", "term of")
            . " $first\n"
            . Msg::color("green", "=> ");
}

# take in array reference of term and definition and return prompt and answer.
sub ppt {
    # array reference and whether term first or definition first
    my ($q, $a) = @_;
    my ($first, $second, $prompt);
    if ($a eq "term") {
        # term first only.
        ($first, $second) = $q->@*;
        $prompt = _tf $first,$second;
    } elsif ($a eq "def") {
        # definition first only.
        ($second, $first) = $q->@*;
        $prompt = _df $first,$second;
    } else {
        # choose randomly.
        if (rand 1 < 0.5) {
            ($second, $first) = $q->@*;
            $prompt = _df $first,$second;
        } else {
            ($first,$second) = $q->@*;
            $prompt = _tf $first,$second;
        }
    }
	# get rid of all whitespace in the beginning and end.
	$second =~ s/\s*$//;
	$second =~ s/^\s*//;
    return ($prompt, $second);
}

# generate regular expression.
sub gre {
    my ($txt) = @_;
    my $res = "^\\s*";
    foreach my $c (split //, $txt) {
        my $x = (ord $c >= 97) ? (uc $c) : (lc $c);
        $res = $res . (($c ne $x) ? "[$c,$x]" : quotemeta $c);
    }
    return "$res\\s*\$";
}

# read, evaluate, print
sub rep {
    my ($ar, $term, $test) = @_;
    my ($prompt, $answer) = ppt ($ar, $term);
    # prompt
    print $prompt;
    # capture response and respond.
    my $response = <<>>;
    # exit if ^D pressed.
    if (! defined $response) {
        print "\n";
        return undef;
    }
    chomp $response;
    my $re = gre $answer;
    if ($response =~ /$re/) {
        if (! $test) {
            print Msg::color("green", "correct") . "\n\n";
        }
        return 1;
    } else {
        if (! $test) {
            print Msg::color("red", "incorrect") . "\n";
            print "The correct answer is '$answer'\n\n";
        }
        return 0;
    }
}


1;
