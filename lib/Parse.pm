use strict;
use warnings;
use diagnostics;
use feature 'say';

# Module for Opening Files, parsing with separator, etc.
package Parse;

use File::Basename;
use lib dirname(__FILE__);
use Msg;

use Exporter qw(import);
our @EXPORT = qw(parse_file);

sub parse_line {
    my ($number, $line, $sep) = @_;
	# get rid of everything after comments.
    my @a = split /\s*$sep\s*/, $line;
    if (scalar @a != 2) {
        Msg::warn <<EOF;
Unparsable with separator $sep (will be ignored).
$number | $line

EOF
        return ();
    }
    return @a;
}

sub sep {
	my ($line, $sep) = @_;
	if (
		$line =~ /^\s*\$\s*[s,S][e,E][p,P]\s*.*\s*$/ ||
		$line =~ /^\s*\$\s*[p,P]\s*.*\s*$/
	) {
		$line =~ s/^\s*\$\s*[s,S][e,E][p,P]\s*//;
		$line =~ s/^\s*\$\s*[p,P]\s*//;
		$line =~ s/\s*$//;
		$$sep = quotemeta $line;
		return 1;
	}
	return 0;
}

sub include {
	my ($line, $include) = @_;
	# INCLUDE command
	if (
		$line =~ /^\s*\$\s*[e,E][x,X][c,C][l,L][u,U][d,D][e,E]\s*$/ ||
		$line =~ /^\s*\$\s*[e,E]\s*$/
	) {
		$$include = 0;
		return 1;
	} 
	# EXCLUDE command
	elsif (
		$line =~ /^\s*\$\s*[i,I][n,N][c,C][l,L][u,U][d,D][e,E]\s*$/ ||
		$line =~ /^\s*\$\s*[i,I]\s*$/
	) {
		$$include = 1;
		return 1;
	}
	return 0;
}

sub star {
	my ($line, $starred) = @_;
	# STAR
	if (
		$line =~ /^\s*\$\s*[s,S][t,T][a,A][r,R]\s*$/ ||
		$line =~ /^\s*\$\s*[s,S]\s*$/
	) {
		$$starred = 1;
		return 1;
	}
	# ALL
	elsif (
		$line =~ /^\s*\$\s*[a,A][l,L][l,L]\s*$/ ||
		$line =~ /^\s*\$\s*[a,A]\s*$/
	) {
		$$starred = 0;
		return 1;
	}
	return 0;
}

sub undef_function {
	my ($number, $line) = @_;
	if ($line =~ /^\s*\$/) {
		Msg::warn <<EOF;
Unknown function given at line $number (will be ignored).
$number | $line

The current functions are
SEP <characters> | P <characters> - for changing separators.
INCLUDE | I - include all words below (unless another EXCLUDE).
EXCLUDE | E - exclude all words below (unless another INCLUDE).
STAR | S - starred words only.
ALL | A - include all words, regardless of if they are starred.

EOF
		return 1;
	}
	return 0;
}

# returns an array reference which contains all terms and references.
# starred is if they only want to study starred words in set.
sub parse_file {
    my $filename = shift or die "no filename given.";
    my $starred = shift || 0;
    my $sep = quotemeta (shift || ",");
    if (! -r $filename) {
        Msg::error "$filename does not exist or is not readable.";
        return;
    }
    my $fh;
    open ($fh, "<", $filename) or die "Impossible.";
	my $include = 1;
    my $ln = 0;
    my @res = ();
    while (my $line = <$fh>) {
        chomp $line;
        $ln++;
		# get rid of comment strings
		
		$line =~ s/#.*$//;
		# handle all functions
		if (sep $line, \$sep) { next;}
		if (include $line, \$include) {next;}
		if (star $line, \$starred) {next;}
		if (undef_function ($ln, $line)) {next;};
		
        # conditions for skipping line (comment or not starred).
        if ($line =~ /^\s*$/ || ! $include) { next; }
        if ($starred && !($line =~ /^\s*\*/)) {next;}

        # get rid of stars at beginning if required.
        $line =~ s/^\s*\*\s*//;
        # parse line
        my @a = parse_line($ln, $line, $sep);
        if (@a) {
            my ($t, $d) = @a;
            $t = lc $t;
            $d = lc $d;
            push @res, [$t, $d];
        }
    }
    return \@res;
}



1;
