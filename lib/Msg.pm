use strict;
use warnings;
use diagnostics;

package Msg;

sub color {
    my ($c, $text) = @_;
    my $codes = {
        "red"=> 31,
        "green"=> 32,
        "yellow"=> 33,
        "blue"=> 34,
        "purple"=> 35
    };
    if (! exists $codes->{$c}) { die "no such color $c" }
    return "\e[1;$codes->{$c}m$text\e[0m"
}

sub warn {
    my ($text) = @_;
    print color("yellow", "warning:")." $text\n";
}

sub error {
    my ($text) = @_;
    print color("red", "error:")." $text\n";
}

1;
