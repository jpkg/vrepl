# VRepl

Simple  Term and Definition REPL to be used on the CLI.

## Installation

```sh
git clone https://github.com/junikimm717/vrem.git
cd vrem/env
./build.pl
```

If your shell is supported, the script will automatically ask you if you want
to append the created env.sh file to your shellrc. Otherwise, you will have to
manually source it.

## Writing a file for input

vrem accepts text files with term/definition pairs. Although the default
separator is a comma, the `--sep` option allows one to manually change the
separator used. vrem will ignore any whitespace between the separator.

### Starring terms in a file

In order to star a term, simply put an asterisk before your term and definition
as shown below. Whitespace between and after the asterisk will be eliminated by
the regular expression.

```txt
* (term),(definition)
```

By Default, vrem will load all terms. If you wish to only study starred terms,
use the `--star` option to only load starred terms into the REPL.

### Comments

Comments begin with a `#`, and any part of the line after this character is
ignored.

### Commands

The `$` operator allows you to use commands to directly modify settings inside
of the text file. Note that all of these commands work in either upper or
lowercase, including the aliases.

The current commands are

- `$ (SEP|P) <separator>` - allows you to define a new separator to use in
below words.
- `$ (INCLUDE|I)` - include terms below in the repl (until next `EXCLUDE`).
- `$ (EXCLUDE|E)` - exclude terms below in the repl (until next `INCLUDE`).
- `$ (STAR|S)` - include starred terms only.
- `$ (ALL|A)` - include all terms, including non-starred ones.
